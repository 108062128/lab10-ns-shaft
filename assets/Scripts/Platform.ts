import Player from "./Player";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Platform extends cc.Component {
    @property({ type: cc.AudioClip })
    soundEffect: cc.AudioClip = null;

    protected isTouched: boolean = false;

    private anim: cc.Animation = null;

    private animState: cc.AnimationState = null;

    private highestPos: number = 118;

    private moveSpeed: number = 100;

    private springVelocity: number = 320;

    private player : cc.Node = null;

    start() {
        this.anim = this.getComponent(cc.Animation);
        this.player = cc.find('Canvas/Game/player');

        if (this.node.name == "Conveyor") {
            this.node.scaleX = Math.random() >= 0.5 ? 1 : -1;
            this.moveSpeed *= this.node.scaleX;
        }
    }

    reset() {
        this.isTouched = false;
    }

    update(dt) {
        if ( (this.node.y - this.highestPos >= 0 && this.node.y - this.highestPos < 100) || this.node.position.y - this.node.height/2 >= this.player.position.y + this.player.height/2){
            this.getComponent(cc.PhysicsBoxCollider).enabled = false;
        }
        else this.getComponent(cc.PhysicsBoxCollider).enabled = true;
    }

    playAnim() {
        if (this.anim) this.animState = this.anim.play();
    }

    getAnimState() {
        if (this.animState) return this.animState;
    }

    platformDestroy() {
        cc.log(this.node.name + " Platform destory.");
        this.node.destroy();
    }

    onBeginContact(contact : cc.PhysicsContact, selfCollider : cc.Collider, otherCollider : cc.Collider){

        if (contact.getWorldManifold().normal.y == 1 && contact.getWorldManifold().normal.x == 0){
        }
        else{
             this.getComponent(cc.PhysicsBoxCollider).destroy();
             return;
            }

        if (otherCollider.node.name == 'upperBound'){
            this.platformDestroy();
            return;
        }

        switch(this.node.name){
            case "Normal":
                if (!this.isTouched){
                    cc.audioEngine.playEffect(this.soundEffect, false);
                }
                break;
            case "Fake":
                if (!this.isTouched){
                    cc.audioEngine.playEffect(this.soundEffect, false);
                    this.scheduleOnce(()=>{
                        this.playAnim();
                        cc.tween(this.node)
                            .call(()=>{
                                this.node.getComponent(cc.RigidBody).active = false;
                            })
                            .to(0.5, {position : cc.v2(this.node.position.x, this.node.position.y + 30)})
                            .call(()=>{
                                this.node.getComponent(cc.RigidBody).active = true;
                            })
                        .start();
                    }, 0.2);
                }
                break;
            case "Nails":
                if (!this.isTouched){
                    cc.audioEngine.playEffect(this.soundEffect, false);
                }
                break;
            case "Trampoline":
                this.playAnim();
                cc.audioEngine.playEffect(this.soundEffect, false);
                otherCollider.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, this.springVelocity);
                break;
            case "Conveyor":
                if (!this.isTouched){
                    cc.audioEngine.playEffect(this.soundEffect, false);
                }
                otherCollider.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.moveSpeed, 0);
                break;
        }

        var player = otherCollider.getComponent(Player);
        if (this.node.name != 'Nails'){
            if (this.node.name == 'Trampoline'){
                player.playerRecover();
            }
            else if (!this.isTouched)
                player.playerRecover();
        }
        else {
            if (!this.isTouched)
                player.playerDamage();
        }

        this.isTouched = true;
    }

    onPostSolve(contact : cc.PhysicsContact, selfCollider : cc.Collider, otherCollider : cc.Collider){
        if (this.node.name == "Conveyor"){
            otherCollider.getComponent(Player).getComponent(cc.RigidBody).linearVelocity = cc.v2(this.moveSpeed, 0);
        }
    }

    onEndContact(contact : cc.PhysicsContact, selfCollider : cc.Collider, otherCollider : cc.Collider){
        if (this.node.name == 'Conveyor'){
            var k = otherCollider.getComponent(Player).getComponent(cc.RigidBody);
            k.linearVelocity = cc.v2(0, k.linearVelocity.y);
        }
    }

    // ===================== TODO =====================
    // 1. In the physics lecture, we know that Cocos Creator
    //    provides four contact callbacks. You need to use callbacks to
    //    design different behaviors for different platforms.
    //
    //    Hints: The callbacks are "onBeginContact", "onEndContact", "onPreSolve", "onPostSolve".

    // 3. "Trampoline" and "Fake" need to play animation when the player touches them.
    //    TAs have finished the animation functions, "playAnim" & "getAnimState", for you.
    //    You can directly call "playAnim" to play animation, and call "getAnimState" to get the current animation state.
    //    You have to play animations at the proper timing.
    //
    // 7. All the platforms have only "upside" collision. You have to prevent the collisions from the other directions.
    //
    //    Hints: You can use "contact.getWorldManifold().normal" to judge collision direction.
    //
    // ================================================
}
