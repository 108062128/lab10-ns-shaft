(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/Scripts/Platform.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, 'a3e6dBne/JI4rscTjPZ8hyF', 'Platform', __filename);
// Scripts/Platform.ts

Object.defineProperty(exports, "__esModule", { value: true });
var Player_1 = require("./Player");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Platform = /** @class */ (function (_super) {
    __extends(Platform, _super);
    function Platform() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.soundEffect = null;
        _this.isTouched = false;
        _this.anim = null;
        _this.animState = null;
        _this.highestPos = 118;
        _this.moveSpeed = 100;
        _this.springVelocity = 320;
        _this.player = null;
        return _this;
        // ===================== TODO =====================
        // 1. In the physics lecture, we know that Cocos Creator
        //    provides four contact callbacks. You need to use callbacks to
        //    design different behaviors for different platforms.
        //
        //    Hints: The callbacks are "onBeginContact", "onEndContact", "onPreSolve", "onPostSolve".
        // 3. "Trampoline" and "Fake" need to play animation when the player touches them.
        //    TAs have finished the animation functions, "playAnim" & "getAnimState", for you.
        //    You can directly call "playAnim" to play animation, and call "getAnimState" to get the current animation state.
        //    You have to play animations at the proper timing.
        //
        // 7. All the platforms have only "upside" collision. You have to prevent the collisions from the other directions.
        //
        //    Hints: You can use "contact.getWorldManifold().normal" to judge collision direction.
        //
        // ================================================
    }
    Platform.prototype.start = function () {
        this.anim = this.getComponent(cc.Animation);
        this.player = cc.find('Canvas/Game/player');
        if (this.node.name == "Conveyor") {
            this.node.scaleX = Math.random() >= 0.5 ? 1 : -1;
            this.moveSpeed *= this.node.scaleX;
        }
    };
    Platform.prototype.reset = function () {
        this.isTouched = false;
    };
    Platform.prototype.update = function (dt) {
        if ((this.node.y - this.highestPos >= 0 && this.node.y - this.highestPos < 100) || this.node.position.y - this.node.height / 2 >= this.player.position.y + this.player.height / 2) {
            this.getComponent(cc.PhysicsBoxCollider).enabled = false;
        }
        else
            this.getComponent(cc.PhysicsBoxCollider).enabled = true;
    };
    Platform.prototype.playAnim = function () {
        if (this.anim)
            this.animState = this.anim.play();
    };
    Platform.prototype.getAnimState = function () {
        if (this.animState)
            return this.animState;
    };
    Platform.prototype.platformDestroy = function () {
        cc.log(this.node.name + " Platform destory.");
        this.node.destroy();
    };
    Platform.prototype.onBeginContact = function (contact, selfCollider, otherCollider) {
        var _this = this;
        if (contact.getWorldManifold().normal.y == 1 && contact.getWorldManifold().normal.x == 0) {
        }
        else {
            this.getComponent(cc.PhysicsBoxCollider).destroy();
            return;
        }
        if (otherCollider.node.name == 'upperBound') {
            this.platformDestroy();
            return;
        }
        switch (this.node.name) {
            case "Normal":
                if (!this.isTouched) {
                    cc.audioEngine.playEffect(this.soundEffect, false);
                }
                break;
            case "Fake":
                if (!this.isTouched) {
                    cc.audioEngine.playEffect(this.soundEffect, false);
                    this.scheduleOnce(function () {
                        _this.playAnim();
                        cc.tween(_this.node)
                            .call(function () {
                            _this.node.getComponent(cc.RigidBody).active = false;
                        })
                            .to(0.5, { position: cc.v2(_this.node.position.x, _this.node.position.y + 30) })
                            .call(function () {
                            _this.node.getComponent(cc.RigidBody).active = true;
                        })
                            .start();
                    }, 0.2);
                }
                break;
            case "Nails":
                if (!this.isTouched) {
                    cc.audioEngine.playEffect(this.soundEffect, false);
                }
                break;
            case "Trampoline":
                this.playAnim();
                cc.audioEngine.playEffect(this.soundEffect, false);
                otherCollider.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, this.springVelocity);
                break;
            case "Conveyor":
                if (!this.isTouched) {
                    cc.audioEngine.playEffect(this.soundEffect, false);
                }
                otherCollider.getComponent(cc.RigidBody).linearVelocity = cc.v2(this.moveSpeed, 0);
                break;
        }
        var player = otherCollider.getComponent(Player_1.default);
        if (this.node.name != 'Nails') {
            if (this.node.name == 'Trampoline') {
                player.playerRecover();
            }
            else if (!this.isTouched)
                player.playerRecover();
        }
        else {
            if (!this.isTouched)
                player.playerDamage();
        }
        this.isTouched = true;
    };
    Platform.prototype.onPostSolve = function (contact, selfCollider, otherCollider) {
        if (this.node.name == "Conveyor") {
            otherCollider.getComponent(Player_1.default).getComponent(cc.RigidBody).linearVelocity = cc.v2(this.moveSpeed, 0);
        }
    };
    Platform.prototype.onEndContact = function (contact, selfCollider, otherCollider) {
        if (this.node.name == 'Conveyor') {
            var k = otherCollider.getComponent(Player_1.default).getComponent(cc.RigidBody);
            k.linearVelocity = cc.v2(0, k.linearVelocity.y);
        }
    };
    __decorate([
        property({ type: cc.AudioClip })
    ], Platform.prototype, "soundEffect", void 0);
    Platform = __decorate([
        ccclass
    ], Platform);
    return Platform;
}(cc.Component));
exports.default = Platform;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=Platform.js.map
        